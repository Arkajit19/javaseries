package areashape;

public class Shapearea {
    public static void main(String[] args) {
        double a1=area(45.876);
        double a2=area(34);
        double a3=area(12);
        double a4=area(45.34,23.56,29.65);
        System.out.println("Area of circle is : "+a1);
        System.out.println("Area of square is : "+a2);
        System.out.println("Area of rectangle is : "+a3);
        System.out.println("Area of triangle is : "+a4);

    }

    public static double area(double a ,double b){

        double c=a*b;
        return c;

    }
    public static double area(int d1){

        double e=d1*d1;
        return e;

    }
    public static double area(double r1){

        final double pi=3.14;
        double k=pi*r1*r1;
        return k;

    }

    public static double area(double m,double n, double p ){

        double s=((m+n+p)/2);
        double t1=(s*(s-m)*(s-n)*(s-p));
        double g1=Math.sqrt(t1);
        return g1;
    }

}

