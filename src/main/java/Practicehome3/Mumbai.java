package Practicehome3;

import java.util.HashSet;
import java.util.Iterator;

public class Mumbai {
    public static void main(String[] args) {
        HashSet<String>st34=new HashSet<>();
        st34.add("Aman");
        st34.add("Kiran");
        st34.add("Bimal");
        st34.add("Chetan");
        st34.add("Deban");
        st34.add("Jivan");
//        Iterator<String>it2= st34.iterator();
//        while (it2.hasNext()){
//
//            System.out.println(it2.next());
//        }
        System.out.println("Before list : ");
        for (String l1: st34){

            System.out.println(l1);
        }
        System.out.println("--------------------------------------------------------------------------------------");
        System.out.println("After removing ");
        st34.remove("Bimal");
        for (String l2:st34){

            System.out.println(l2);
        }
        System.out.println("----------------------------------------------------------------------------------------");
        System.out.println("New update : ");
        HashSet<String>str67=new HashSet<>();
        str67.add("Karim");
        str67.add("House");
        str67.add("Lone");
        st34.addAll(str67);
        for (String h1:st34){

            System.out.println(h1);
        }
        System.out.println("---------------------------------------------------------------------------------------");
        st34.removeAll(str67);
        for (String p1: st34){

            System.out.println(p1);
        }
    }
}
