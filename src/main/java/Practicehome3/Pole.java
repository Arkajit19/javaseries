package Practicehome3;

import java.util.HashSet;

public class Pole {
    public static void main(String[] args) {

        HashSet<String> st56=new HashSet<>();
        st56.add("Ajit");
        st56.add("Bimal");
        st56.add("Rabi");
        st56.add("Vimal");
        st56.add("Home");
        st56.add("Samal");

        System.out.println("Before : ");

        for (String j1:st56){

            System.out.println(j1);
        }
        System.out.println("-------------------------------------------------------------------");

        System.out.println("After : ");

        st56.clear();
        for (String h1: st56){

            System.out.println(h1);
        }
    }
}
