package Practicehome3;

import java.util.HashSet;

public class Pune {
    public static void main(String[] args) {

        HashSet<String>st56=new HashSet<>();
        st56.add("Ajit");
        st56.add("Bimal");
        st56.add("Rabi");
        st56.add("Vimal");
        st56.add("Home");
        st56.add("Samal");
        for (String p1:st56){

            System.out.println(p1);
        }
        System.out.println("----------------------------------------------------------------------------");
        System.out.println("After removing perticular element : ");
        st56.removeIf(str->str.contains("Vimal"));
        for (String t1:st56){
            System.out.println(t1);
        }
    }
}
