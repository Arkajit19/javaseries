package Practicehome;

public class MaxMin {
    public static void main(String[] args) {

        int[] list2={45,76,98,34,12,49,92,34};

        int k=max(list2);
        int k1=min(list2);
        System.out.println("The max element is : "+k);
        System.out.println("The min element is : "+k1);
    }

    public static int max(int arr[]){

        int  elem_max=arr[0];
        for (int a=1;a<arr.length;a++){

            if (elem_max<arr[a]){

                elem_max=arr[a];
            }
        }
     return elem_max;
    }
    public static int min(int arr1[]){

        int  elem_min=arr1[0];
        for (int a=1;a<arr1.length;a++){

            if (elem_min>arr1[a]){

                elem_min=arr1[a];
            }
        }
        return elem_min;
    }

}
