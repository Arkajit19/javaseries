package Practicenewhome2;

import java.util.ArrayList;
import java.util.Collections;

public class SimpleUpdate {
    public static void main(String[] args) {
        ArrayList<Student>lst9=new ArrayList<>();
        lst9.add(new Student(67,"Amit",20));
        lst9.add(new Student(79,"Vijay",24));
        lst9.add(new Student(78,"Bimal",19));
        lst9.add(new Student(69,"Kamal",25));
        System.out.println("Sorting by age : ");
        Collections.sort(lst9,new UpdatedageComparator());
        for (Student kl1:lst9){

            System.out.println("Marks is "+kl1.marks+" . Name is "+kl1.name+" . Age is "+kl1.age);
        }
        System.out.println("---------------------------------------------------------------------");
        System.out.println("Sorting by name : ");
        Collections.sort(lst9,new Updatednamecomparator());
        for (Student lk2: lst9){

            System.out.println("Marks is "+lk2.marks+" . Name is "+lk2.name+" . Age is "+lk2.age);
        }
    }
}
