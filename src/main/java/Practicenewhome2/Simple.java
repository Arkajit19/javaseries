package Practicenewhome2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class Simple {

    public static void main(String[] args) {

        ArrayList<Student>lst8=new ArrayList<>();
        lst8.add(new Student(89,"Bikash",21));
        lst8.add(new Student(81,"Amit",20));
        lst8.add(new Student(84,"Rakesh",19));
        lst8.add(new Student(86,"Tias",26));
        System.out.println("Sorting by name : ");
        Collections.sort(lst8,new NameComparator());
        for (Student k1: lst8){
            System.out.println("Marks is "+k1.marks+". Name is "+k1.name+". Age is "+k1.age);
        }
        System.out.println("-------------------------------------------------------------------------------------------------");
        System.out.println("Sorting by age : ");
        Collections.sort(lst8,new AgeComparator());
        for (Student k2: lst8){

            System.out.println("Marks is "+k2.marks+". Name is "+k2.name+". Age is "+k2.age);

        }

    }
}