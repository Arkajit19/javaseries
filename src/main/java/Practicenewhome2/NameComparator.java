package Practicenewhome2;

import java.util.Comparator;

public class NameComparator implements Comparator {

    public int compare(Object obj4, Object obj5){

        Student st3=(Student) obj4;
        Student st4=(Student) obj5;
        return st3.name.compareTo(st4.name);
    }
}
